﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoreManagementSystem.Models
{
    public class Artist
    {
        public int ArtistId { get; set; }
        public String Name { get; set; }
    }
}