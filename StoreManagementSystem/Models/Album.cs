﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StoreManagementSystem.Models
{
    // Bind – Lists fields to exclude or include when binding parameter or form values to model properties
    [Bind(Exclude = "AlbumId")]
    public class Album
    {
        //ScaffoldColumn – Allows hiding fields from editor forms
        [ScaffoldColumn(false)]
        public int AlbumId { get; set; }

        //DisplayName – Defines the text to use on form fields and validation messages
        [DisplayName("Genre")]
        public int GenreId { get; set; }

        [DisplayName("Artist")]
        public int ArtistId { get; set; }

        //StringLength – Defines a maximum length for a string field
        //Required – Indicates that the property is a required field
        [Required(ErrorMessage = "An Album Title is required")]
        [StringLength(160)]
        public string Title { get; set; }

        //Range – Gives a maximum and minimum value for a numeric field
        [Range(0.01, 100.00, ErrorMessage = "Price must be between 0.01 and 100.00")]
        public decimal Price { get; set; }

        //StringLength – Defines a maximum length for a string field
        [DisplayName("Album Art URL")]
        [StringLength(1024)]
        public string AlbumArtUrl { get; set; }

        //Virtual - This allows Entity Framework to lazy-load them as necessary.
        public virtual Genre Genre { get; set; }

        public virtual Artist Artist { get; set; }
    }
}